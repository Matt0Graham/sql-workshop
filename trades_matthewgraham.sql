
CREATE PROCEDURE gettradertrades @name nvarchar(30)
AS
SELECT t.id, tr.stock, tr.buy, tr.size, tr.price FROM trader AS t 
INNER JOIN position AS p ON t.id = p.trader_ID
INNER JOIN trade AS tr ON p.closing_trade_ID = tr.id 
INNER JOIN trade ON p.opening_trade_ID = trade.id
WHERE t.first_name = @name

DROP Procedure gettradertrades;

EXEC gettradertrades 
@name='toby'; 

