SELECT * FROM dbo.bond WHERE dbo.bond.CUSIP = '28717RH95';

--2 
SELECT * FROM dbo.bond ORDER BY maturity ASC;

--3
SELECT SUM(dbo.bond.quantity * dbo.bond.price) FROM dbo.bond

--4

SELECT  bond.id , SUM(quantity * (bond.coupon/100)) as Returns FROM bond GROUP BY bond.id
--5
SELECT * FROM dbo.bond INNER JOIN rating ON rating.rating = bond.rating
WHERE rating.ordinal <= 3;

--6 
SELECT rating, AVG (price) as AVGPRICE , AVG (coupon) AS AVGCOUPON from bond GROUP BY rating

--7
SELECT id, (coupon-price) AS ACTUAL, r.expected_yield, (r.expected_yield -(coupon-price)) AS diff FROM bond 
INNER JOIN rating AS r on bond.id = r.ordinal ORDER BY diff DESC
